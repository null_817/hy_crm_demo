<?php
namespace app\common\model;
use think\Model;

class Page {



    /**
     * 数据集合
     * @var array
     */
    public $list;

    /**
     * 当前页数
     * @var int
     */
    public $page;

    /**
     * 每页显示的条数
     * @var int
     */
    public $list_rows;

    /**
     * 总共页数
     * @var int
     */
    public $total_page;

    /**
     * 总条数
     * @var int
     */
    public $total_count;


    /**
     * 当前页的条数
     * @var int
     */
    public $page_rows;

    /**
     * Page constructor.
     * @param int $total_count 总条数
     * @param int $list_rows   每页显示的条数
     * @param int $page        当前页
     */
    public function __construct($total_count,$list_rows,$page = 1) {
        $this->total_count = $total_count;
        $this->list_rows   = $list_rows;
        $this->total_page  = (int)ceil($total_count/$list_rows);
        $this->page        = $page;
    }

    public function __get( $name ) {
        return $this->{$name};
    }

    public function __call($name, $arguments) {
        // TODO: Implement __call() method.
    }

    public function getList(){
        return $this->list;
    }

    public function changeList($list){
        if(!empty($list) && $list['0'] instanceof Model){
            foreach ($list as $key => $val)
                $list[$key] = $val->toArray();
        }
        $this->list = $list;
        $this->page_rows = count($list);
        return $this;
    }
}