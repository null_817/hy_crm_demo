<?php
namespace app\common\model;



use app\common\error\CommonError;
use app\common\error\IError;
use think\Model;
use think\Validate;

class Base extends Model {
    protected $_condition;
    protected $_plus_condition;
    protected $_order;
    protected $_list_rows;
    protected $_group;
    protected $with;
    protected $_plus_field;
    protected $_limit;
    /**
     * @var IError
     */
    protected $error;
    protected $status_field = "stauts";
    //验证
    protected $rule = [];
    //验证错误信息
    protected $rule_msg = [];


    public function __construct( $data = [] ) {
        parent::__construct( $data );
    }

    public function setError($msg , $code = -6) {
        if($msg instanceof IError)
            $error = $msg;
        else
            $error = CommonError::init(["errCode" => $code,"errMsg" => $msg]);
        $this->error = $error;
        return false;
    }

    public function getError($is_string = true) {
        return $is_string?$this->error->getErrMsg():$this->error->getError();
    }


    //获取器
    public function getCreateTimeAttr( $value ) {
        return new_date( $value );
    }

    public function getUpdateTimeAttr( $value ) {
        return new_date( $value );
    }


    /**
     * get condition for where
     * @param array $condition
     * @return array
     */
    private function conditionHandle( array $condition = [] ) {
        if ( $this->_condition )
            return $this->_condition;
        else if ( $this->_plus_condition )
            return array_merge( $this->_plus_condition, $condition );
        return $condition;
    }

    private function withHandle( $with = [] ) {
        ( !empty( $with ) || !empty( $this->with ) ) && $this->field = []; //兼容ThinkPHP 5.0 model 重复查询的问题
        if ( $this->with )
            return $this->with;
        return $with;
    }

    /**
     * get sort for condition
     * @param string $order
     * @return mixed
     */
    private function orderHandle( $order = 'id desc' ) {
        if ( $this->_order )
            return $this->_order;
        return $order;
    }

    /**
     * get view field
     * @param array $field
     * @return array
     */
    private function fieldHandle( $field = [] ) {
        if( $this->_plus_field )
            return array_merge( $this->field , $this->_plus_field );
        if ( $this->field )
            return $this->field;
        return $field;
    }

    /**
     * get view field
     * @param int $limit
     * @return int
     */
    private function limitHandle( $limit = 3000 ) {
        if ( $this->_limit )
            return $this->_limit;
        return $limit;
    }

    private function groupHandle( $group = '' ) {
        if ( $this->_group )
            return $this->_group;
        return $group;
    }

    /**
     * setting sort for condition
     * @param $order
     * @return $this
     * @auth albert
     */
    public function changeOrder( $order ) {
        $this->_order = $order;
        return $this;
    }

    /**
     * setting plus_condition
     * @param array $where
     */
    public function changePlusCondition( $where = [] ) {
        $this->_plus_condition = $where;
    }

    public function changePlusField( $field = [] ){
        $this->_plus_field = $field;
        return $this;
    }

    /**
     * setting condition
     * @param array $condition
     * @return $this
     * @auth albert
     */
    public function changeCondition( $condition = [] ) {
        $this->_condition = $condition;
        return $this;
    }

    public function changeStatusField( $field = '' ){
        $this->status_field = $field;
        return $this;
    }

    /**
     * clear sort for condition
     * @return $this
     */
    public function clearOrder() {
        unset( $this->_order );
        return $this;
    }

    /**
     * setting with
     * @param array $with
     * @return $this
     */
    public function changeWith( $with = [] ) {
        $this->with = $with;
        return $this;
    }

    /**
     * setting view fields
     * @param $fields
     * @return $this
     */
    public function changeFields( $fields = [] ) {
        $this->field = $fields;
        return $this;
    }

    public function changeGroup( $group ) {
        $this->_group = $group;
        return $this;
    }

    /**
     * setting page list rows
     * @param int $list_rows
     * @return bool
     * @throws ErrorException
     */
    public function setListRows( $list_rows = 15 ) {
        if ( $list_rows > 0 )
            return $this->_list_rows = $list_rows;
        //throw new ErrorException();
    }

    /**
     * 设置不显示的字段
     * @param array $not_view
     * @return $this
     */
    public function settingNotViewField( $not_view = [] ) {
        $field = $this->field;
        if ( !empty( $field ) )
            $field = array_diff( $field, $not_view );
        return $this->changeFields( $field );
    }

    /**
     * 设置不分页时查询数量
     * @param $limit
     * @return $this
     */
    public function changeLimit( $limit ) {
        $this->_limit = $limit;
        return $this;
    }


    /**
     * 列表查询
     * list select
     * @param array $condition
     * @param int|bool $page false 不分页  int:>0分页
     * @return string|Page|array|Model
     * @author albert
     */
    public function getList( $condition, $page = 1 ) {
        $where = [];
        if ( is_array( $condition ) )
            $where = $condition;
        if ( in_array( $this->status_field , $this->field ) && !( array_key_exists( $this->status_field , $where ) ) ) {
            $where[ $this->status_field ] = 1;  //如果没指定status 并且 字段中存在status 则默认查找status为1的字段
        }
        $where = $this->conditionHandle( $where );
        if ( $page === false || $page < 1 ) {
            return $this->afterDecorator(self::where( $where )
                ->with( $this->withHandle( [] ) )
                ->field( $this->fieldHandle() )
                ->order( $this->orderHandle( $this->pk ?: "id" . '  desc' ) )
                ->limit( $this->limitHandle( 30000 ) )
                ->group( $this->groupHandle() )
                ->select());
        }
        $count = self::where( $where )->with( $this->withHandle( [] ) )->count();
        $page = new Page( $count, empty( $this->_list_rows ) ? 8 : $this->_list_rows, $page );
        $page->changeList(
            $this->afterDecorator(self::where(  $where  )
                ->with( $this->withHandle( [] ) )
                ->field( $this->fieldHandle() )
                ->order( $this->orderHandle( $this->pk ?: "id" . ' desc' ) )
                ->limit( ( $page->page - 1 ) * $page->list_rows, $page->list_rows )
                ->group( $this->groupHandle() )
                ->select())
        );
//        dump( $page );
        //Cache::set('last_sql',static::getLastSql());
        return $page;
    }


    /**
     * 查询单个
     * select one
     * @param array $condition
     * @return array|false|\PDOStatement|string|Model
     */
    public function getOne( $condition = [] ) {
        $where = [];
        if ( in_array( $this->status_field , $this->field ) && !( array_key_exists( $this->status_field , $condition ) ) )
            $where[ $this->status_field ] = 1;
        if ( is_array( $condition ) )
            $where = array_merge( $where, $condition );
        $where = $this->conditionHandle( $where );
        return$this->afterDecorator(self::where( $where )->with( $this->withHandle( [] ) )->field( $this->fieldHandle() )->find());
    }

    /**
     * 装饰器
     * @param array $data
     * @return array|Model|Collection
     */
    public function afterDecorator( $data = [] ){
        if (empty($data))
            return $data;
        if($data instanceof Page){
            foreach($data->list as $key => $value){
                $value->append($this->append);
                $data->list[$key] = $value;
            }
        } else if(is_array($data)){
            foreach($data as $key => $value){
                $value->append($this->append);
                $data[$key] = $value;
            }
        }else{
            $data->append($this->append);
        }
        return $data;
    }


    /**
     * 更新
     * @param $data
     * @return bool|Model
     */
    public function updates($data) {
        $pk = $this->pk ?: 'id';
        $validate = new Validate($this->rule, $this->rule_msg);
        if (empty($data[$pk]))
            return $this->setError("主键丢失");
        $bool = $validate->check($data);
        if ($bool === false)
            return $this->setError($validate->getError());
        if (method_exists($this, 'commonValidate') && $this->commonValidate($data) === false)
            return false; //公共验证
        if (method_exists($this, 'updateValidate') && $this->updateValidate($data) === false)  //验证
            return false;//更新验证
        $this->isUpdate(true)->allowField(true)->save($data,[$pk => $data[$pk]]);
        if(method_exists($this,"updateAfter"))
            $this->updateAfter($data);
        return $this;
    }

    /**
     * 添加
     * @param $data
     * @return bool|mixed
     */
    public function insert($data) {
        $pk = $this->pk ?: 'id';
        $validate = new Validate($this->rule, $this->rule_msg);
        if (empty($data))
            return $this->setError("数据不能为空");
        if (in_array('create_time', $this->field))
            $data['create_time'] = THINK_START_TIME;
        $bool = $validate->check($data);
        if ($bool === false)
            return $this->setError($validate->getError());
        if (method_exists($this, 'commonValidate') && $this->commonValidate($data) === false)
            return false; //公共验证
        if (method_exists($this, 'insertValidate') && $this->insertValidate($data) === false)  //验证
            return false; //插入验证
        $this->isUpdate(false)->allowField(true)->save($data);
        if(method_exists($this,"insertAfter"))
            $this->insertAfter($data);
        return $this->{$pk};
    }

}