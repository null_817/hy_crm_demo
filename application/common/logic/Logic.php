<?php
namespace app\common\logic;

use app\common\error\CommonError;
use app\common\error\IError;

abstract class Logic
{
    /**
     * 错误信息
     * @var IError
     */
    protected $_error;
   
    
    public function __construct($val = ''){
        $this->init($val);
    }


    /**
     * @param $msg
     * @param int $code
     * @return bool
     */
    public function setError($msg,$code = -6){
        if($msg instanceof IError)
            $error = $msg;
        else
            $error = CommonError::init(["errCode" => $code,"errMsg" => $msg]);
        $this->_error = $error;
        return false;
    }
    
    /**
     * 返回错误信息
     * @param string $key 如果为空返回错误对象 为string返回错误描述 为code返回错误码
     * @return \app\common\error\IError|string|array
     */
    public function getError($key = ''){
        if($key == '')
            return $this->_error;
        if($key == 'string')
            return $this->_error->getErrMsg();
        if($key == 'code')
            return $this->_error->getErrCode();
        if($key == 'array')
            return $this->_error->getError();
    }


    protected function returnData( $data = '',$errCode = 0 ) {
        $errMsg = INTERFACE_NAME.'.'.ACTION_NAME.':ok';
        if ( $errCode != 0 )
        {
            $errMsg = $data;
            $data = '';
        }
        return [ 'errCode' => $errCode,'errMsg' => $errMsg,'data' => $data ];
    }

    abstract function init($val = '');
}