<?php
namespace app\common\server;

use traits\think\Instance;

/**
 * @method static getCustomerFollow() 获取客户跟进记录API
 * @method static changeNotify()      通知
 * @method static changBatchNotify()  批量通知多个id
 * @method static issuedNotify() 下发通知门店
 * @method static autoUpdateNode()更新crm_server节点
 * @package app\common\server
 */
final class Server {

    use Instance;
    public function __construct($options = []){
        if(!empty($options['type']))
            $this->type = $options['type'];
        if(!empty($options['async']))
            $this->async= $options['async'];
        if(!empty($options['return_data']))
            $this->return_data = $options['return_data'];
    }
    public function __call($name, $arguments){
        $action = $this->type;
        if(!empty($arguments[0]))
            $this->setParams($arguments[0]);
        $this->action = $name;
        $data = $this->$action();
        if(is_array($data) && $this->return_data){
            return !array_key_exists('data',$data)?null:$data['data'];
        }
        return $data;
    }
    public static function __callStatic($name , $arguments){
        $options = [];
        if(!empty($arguments[1]))
            $options = $arguments[1];
        $instance = self::instance($options);
        return call_user_func_array([$instance,$name],$arguments);
    }
    // 请求url
    protected $api_url_list = [
        'notify' => "http://socket.lif8.com:9502",
        'sys' => "http://crm_server.lif8.com/index.php/api/AdminInterface/rootInterface"
    ];
    protected $api_list = [
        'changeNotify' => [
            'api' => 'notify',
            'action' => 'changeNotify'
        ],
        'changBatchNotify' => [
            'api' => 'notify',
            'action' => 'changBatchNotify',
        ],
        'issuedNotify' => [
            'api' => 'notify',
            'action' => 'issuedNotify'
        ],
        'autoUpdateNode' => [
            'api' => 'sys',
            'action' => 'PublicAction/autoUpdateNode'
        ]
    ];
    protected $success;
    protected $return_data;
    protected $async = false;
    protected $action;
    protected $type = 'jsonp';
    public function changeAction($action){
        $this->action = $action;
        return $this;
    }
    // 请求参数
    protected $param = [];
    //设置参数
    public function setParams( $param ){
        $this->param = $param;
        return $this;
    }

    public function http( $action = '' ){
        if(!empty($action))
            $this->changeAction($action);
        if(empty($this->api_list[$this->action]))
            throw new \Exception("api is not defined");
        $api = $this->api_list[$this->action];
        if(empty($this->api_url_list[$api['api']]))
            throw new \Exception("api url is not defined");
        $url = $this->api_url_list[$api['api']];
        $interface = $api['action'];
        $ch = curl_init();
        $params = [
            'interface' => $interface,
            'params'    => json_encode($this->param)
        ];
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,1);
        $return_transfer = true;
        if($this->async){
//            curl_setopt($ch,CURLOPT_TIMEOUT,3);
            $return_transfer = true;
        }
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, $return_transfer);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    public function jsonp($api = ''){
        $response = $this->http($api);
        $response = str_replace(array('jsonpReturn(',');'),'',$response);  //删除jsonp字符串得到可解析的json
        $response = json_decode($response,true);
        return $response;
    }
    public function json($api = ''){
        $response = $this->http($api);
        $response = json_decode($response,true);
        return $response;
    }
}