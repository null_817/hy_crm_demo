<?php
namespace app\common\error;

interface IError{
    public function getErrCode();
    
    public function setErrCode($err_code = 0);
    
    public function setErrMsg($err_msg  = '');
    
    public function getErrMsg();
    
    public function setError(array $options = []);
    
    public function getError();
}