<?php
namespace app\common\error;

class CommonError implements IError{
    private $__err_code;
    private $__err_msg;
    private static $__instance;
    
    public static function init($options = []){
        if(self::$__instance == null)
            self::$__instance = new self($options);
        return self::$__instance;
    }
    
    private function __construct($options = []) {
        $this->setError($options);
    }
    
    public function getErrCode() {
        return $this->__err_code;
    }
    
    public function getErrMsg() {
        return $this->__err_msg;
    }
    
    public function setErrCode($err_code = 0) {
        $this->__err_code = $err_code;
    }
    
    public function setErrMsg($err_msg = '') {
        $this->__err_msg = $err_msg;
    }
    
    /**
     * 设置错误信息
     * @param array $options
     */
    public function setError(array $options = []) {
        if (isset($options['errCode']))
            $this->__err_code = $options['errCode'];
        if (isset($options['errMsg']))
            $this->__err_msg = $options['errMsg'];
    }
    
    /**
     * 返回错误信息
     * @return array
     */
    public function getError() {
        return [
                "errCode" => $this->__err_code,
                "errMsg"  => $this->__err_msg
        ];
    }
}