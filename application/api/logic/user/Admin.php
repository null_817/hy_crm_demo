<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 10:24
 */

namespace app\api\logic\user;


use app\api\model\User;
use app\common\logic\Logic;

/**
 * Class Admin
 * @method static id() 获取用户ID
 * @method static role_id() 获取用户角色id
 * @method static pid()
 * @method static user_info()
 * @method static store_id()
 * @method static real_name() 真实姓名
 * @method static phone()
 * @package app\api\logic\user
 */
class Admin extends Logic
{
    private static $user_info;

    private static $child = null;

    /**
     * 节点验证
     * @param $interface
     * @param $action
     * @param $user_info
     * @return bool
     */
    static function validationNode($interface, $action)
    {
        return (!empty(self::$user_info['node'][$interface]) && !empty(self::$user_info['node'][$interface]['child'][$action]));
    }

    public static function child()
    {
        if (!empty(self::$child)) {
            return self::$child;
        }
        User::where(['store_id' => self::store_id()])->column('id,pid', 'id');
    }

    public static function __callStatic($name, $arguments)
    {
        if (empty(self::$user_info))
            return '';
        if ($name == 'user_info')
            return self::$user_info;
        return empty(self::$user_info[$name]) ? '' : self::$user_info[$name];
    }

    public static function setUserInfo($info)
    {
        self::$user_info = $info;
    }

    function init($val = '')
    {
        // TODO: Implement init() method.
    }
}