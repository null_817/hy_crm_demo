<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 15:32
 */

namespace app\api\logic\user;

use app\api\model\CustomerInfo;
use app\api\model\CustomerTransfer;
use app\api\model\UserInfo;
use app\common\logic\Logic;
use think\Db;
use traits\think\Instance;

class UserLogic extends Logic
{
    use Instance;

    /**
     * @var UserInfo
     */
    protected $model;

    /**
     * @var CustomerInfo
     */
    protected $CustomerInfo;

    /**
     * @var CustomerTransfer
     */
    protected $CustomerTransfer;


    function init($val = '')
    {
        $this->model            = new UserInfo;
        $this->CustomerInfo     = new CustomerInfo;
        $this->CustomerTransfer = new CustomerTransfer;
    }

    /**
     * 用户信息
     * @return array
     */
    public function getUserInfoLogic()
    {
        $this->model->with(['store']);
        $this->model->changeFields(['id', 'role_id', 'store_id', 'real_name']);
        return ['errCode' => 0, 'errMsg' => INTERFACE_NAME . '.' . ACTION_NAME . ':ok', 'data' => $this->model->getOne(['id' => Admin::id()])];
    }

    /**
     * 我的团队列表
     * @return array
     */
    public function getTeamListLogic()
    {
//        $this->model->changeFields(['role_id', 'pid', 'real_name', 'phone', 'type']);
        $this->model->append(['type_name']);
        $this->model->changePlusCondition([
            'status' => 1
        ]);
        return ['errCode' => 0, 'errMsg' => INTERFACE_NAME . '.' . ACTION_NAME . ':ok', 'data' => $this->model->getList(['store_id' => Admin::store_id()])];
    }

    public function transferCustomerLogic($id)
    {
        $this->model->getOne([]);
    }

    /**
     * 添加成员处理
     * @param $param
     * @return array
     */
    public function getAddMember($param)
    {
        if (Admin::role_id() != 2 && Admin::role_id() != 1) {
            return $this->returnData("权限不足,请让主管操作", -6);
        }
        $phone = $param['phone'];
        $User  = $this->model->where('phone', $phone)->find();
        if (!empty($User)) {
            return $this->returnData("此用户已存在,可以尝试换个手机号码", -6);
        }
        return $this->returnData($this->model->create([
            'role_id' => 3,
            'store_id' => Admin::store_id(),
            'real_name' => $param['name'],
            'phone' => $param['phone'],
            'type' => 2,
            'create_time' => time(),
            'status' => 0
        ]));
    }

    /**
     * 删除成员处理
     * @param $param
     * @return array
     */
    public function getDelMember($param)
    {
        $id      = Admin::id();
        $user_id = $param['id'];
        $role    = Admin::role_id();
        if ($id == $user_id) {
            return $this->returnData("不能删除自己", -6);
        }
        if ($role == 1 || $role == 2) {
            $User           = $this->model->where('id', $user_id)->find();
            $User['status'] = 0;
            $User->save();
            return $this->returnData('亲，该成员已被删除');
        } else {
            return $this->returnData('亲，请让主管进行处理', -6);

        }
    }

    /**
     * 查询可转移用户处理
     * @param $param
     * @return array
     */
    public function getMayShiftCustomer($param)
    {
        $data = $this->model->where('store_id', Admin::store_id())
            ->where('status', 1)
            ->field(['id as value', 'real_name as text'])
            ->select();
        return $this->returnData($data);
    }

}