<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 08:49
 */

namespace app\api\logic\user;


use app\api\model\Login;
use app\api\model\User;
use app\common\logic\Logic;
use think\Cache;
use traits\think\Instance;

class LoginLogic extends Logic
{
    use Instance;
    /**
     * @var User
     */
    protected $user_model;
    /**
     * @var Login
     */
    protected $login_model;
    private   $key = 'bill_server';

    function init($val = '')
    {
        $this->user_model = new User;
        $this->login_model = new Login;
        // TODO: Implement init() method.
    }

    /**
     * 账户密码登陆
     * @param $account
     * @param $password
     * @return array
     */
    public function userLogic($account, $password)
    {
//        $this->login_model->changeFields( ['user_id',''] );
        $login_info = $this->login_model->getOne(['oatuh_id' => $account, 'oatuh_name' => 'account']);
        if (empty($login_info))
            return $this->returnData('账号不存在', -6);
        if (sha1(md5($password . $this->key)) !== $login_info['oatuh_access_toke'])
            return $this->returnData('密码错误', -6);
        return $this->__changLogin($login_info['user_id']);
    }

    /**
     * 验证码登陆
     * @param $phone
     * @param $code
     * @return array
     */
    public function verificationCodeLogin($phone, $code)
    {
//        if(empty($code)){
//            return $this->returnData("验证码错误",-6);
//        }
        $login_info = $this->login_model->where(['oatuh_id' => $phone, 'oatuh_name' => 'account'])->value('user_id');
        if (empty($login_info))
            return $this->returnData('账号不存在', -6);
        if (Cache::get('send_sms' . $phone) != $code)
            return $this->returnData('请输入正确的验证码', -6);
        Cache::set('send_sms' . $phone, null);
        return $this->__changLogin($login_info);
    }

    public function phoneLogin($phone)
    {
        $login_info = $this->login_model->where(['oatuh_id' => $phone, 'oatuh_name' => 'account'])->value('user_id');
        if (empty($login_info))
            return $this->returnData('该用户未开通东家CRM' . $phone, -6);
        return $this->__changLogin($login_info);
    }


    /**
     * 登陆出口
     * @param $user_id
     * @return array|false|\PDOStatement|string|\think\Model
     */
    private function __changLogin($user_id)
    {
        $user_info = $this->user_model->getOne(['id' => $user_id]);
        if (empty($user_info))
            return $this->returnData('用户不存在', -6);
        $user_info['_token_'] = sha1(md5(microtime() . $user_info['user_name'] . $this->key));
        Cache::set('user_info' . $user_id, $user_info, 8640000);
        unset($user_info['role']);
        return $this->returnData($user_info);
    }

}