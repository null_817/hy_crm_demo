<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace app\api\logic;

use app\api\logic\user\Admin;
use app\api\model\CustomerInfo;
use EasyDingTalk\Application;
use EasyDingTalk\Kernel\Messages\Link;
use traits\think\Instance;

class DingTalk
{
    use Instance;

    /**
     * @var Application
     */
    protected $app;

    protected $chat = '';

    protected $userList = [
        '5267166637856552',
        '101221626321049791',
        '141855246926481390'
    ];

    public function __construct()
    {
        $this->app = new Application([
            "corp_id" => config("corpid"),
            "corp_secret" => config("corpsecret"),
        ]);
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getChatId($user_id)
    {
        $data = CustomerInfo::where("id", $user_id)->find();
        if ($data['form_status'] == 1) {
            return false;
        }
        if (empty($data['huihua_id'])) {
            $this->userList[] = DD_USER_ID;
            $huihua = $this->app->chat->create([
                'name' => $data['name'] . '派单跟进反馈（系统自动新建）',
                'owner' => '5267166637856552',
                'useridlist' => $this->userList
            ]);
            $data['huihua_id'] = $huihua['chatid'];
            $this->app->chat->send([
                'chatid' => $huihua['chatid'],
                'msgtype' => 'text',
                'text' => [
                    'content' => '本群为系统自动建立的客户跟进情况汇报会话。当客户跟进情况有变动时，系统会自动建立该会话。当客户无效时会话将自动销毁',
                ],
            ]);

            $data->save([
                'huihua_id' => $data['huihua_id']
            ], ['id' => $user_id]);
        }
        return $data['huihua_id'];
    }

    /**
     * @param $user_id
     * @param $content
     */
    public function send($user_id, $content, $type = 'text')
    {
        $this->userList[] = DD_USER_ID;
        $data = CustomerInfo::where("id", $user_id)->find();

        if ($type == 'img') {
            $msg_content = [
                "title" => $data['name'] . "派单跟进",
                "text" => "##${data['name']}派单跟进 \n ![alt 啊](${content})"
            ];
        } else {
            $msg_content = [
                'message_url' => 'http://sys.lif8.cn/#/app/customer-detail?id=' . $user_id,
                'pc_message_url' => 'http://sys.lif8.cn/#/app/customer-detail?id=' . $user_id,
                'head' => [
                    'bgcolor' => 'FFBBBBBB',
                    'text' => '派单跟进反馈'
                ],
                'body' => [
                    'title' => '',
                    'form' => [
                        [
                            'key' => '姓名',
                            'value' => $data['name']
                        ],
                        [
                            'key' => '手机号',
                            'value' => $data['phone']
                        ],
                        [
                            'key' => '地区',
                            'value' => $data['province_name'] . $data['city_name'] . $data['region_name']
                        ]
                    ],
                    'content' => $content,
                    'author' => Admin::real_name()
                ]
            ];
        }

        $msg_type = $type == 'img' ? 'markdown' : 'oa';
        $result = $this->app->async_message->send([
            'agent_id' => 134960822,
            'userid_list' => implode(',', $this->userList),
            'msgtype' => $msg_type,
            'msgcontent' => json_encode($msg_content),
        ]);


//            ->withReply(new Link([
//            'title' => '派单提醒',
//            'text' => $content,
//            //            'pic_url' => 'static.dingtalk.com/media/lALPBY0V4qovbdjMkMyQ_144_144.png_450x10000q90.jpg',
//            'message_url' => 'http://sys.lif8.cn/#/app/customer-detail?id=' . $user_id
//        ]))->toUser($this->userList)->ofAgent('134960822')->send();
    }

//    public function delete($user_id)
//    {
//        $data = CustomerInfo::where("id", $user_id)->find();
//        if (empty($data['huihua_id'])) {
//            return true;
//        }
//        $chat = $this->app->chat->get($data['huihua_id']);
//        $useridlist = $chat['chat_info']['useridlist'];
//        return $useridlist;
//    }
}