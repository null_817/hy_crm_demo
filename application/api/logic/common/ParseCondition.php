<?php
namespace app\api\logic\common;


use app\common\logic\Logic;
use traits\think\Instance;

class ParseCondition extends Logic {

    use Instance;
    function init($val = ''){
        // TODO: Implement init() method.
        // 解析筛选属性
        // 规则time
    }

    function __construct($table = '')
    {
        $this->table = $table;
        parent::__construct($table);
    }

    protected $condition = [];
    protected $key; //数据表字段名
    protected $table;

    //时间类型
    protected function time($k,$c){
        // 时间有时间区间
        // ?_start = 开始时间
        // ?_end   = 结束时间
        // ?_start && ?_end //
        // create_time
        // create_time_start
        // cteate_tiem_end
        $condition = [];
        $start = $this->key."_start";
        $end   = $this->key."_end";
        if(!empty($c[$start]))
            $this->condition[$k] = ['>=',strtotime($c[$start])];
        if(!empty($c[$end]))
            $this->condition[$k] = ['<=',strtotime($c[$end])];
        if(!empty($c[$start]) && !empty($c[$end]))
            $this->condition[$k] = [ ['>=',strtotime($c[$start])] , ['<=',strtotime($c[$end])] ];
        return $this;
    }

    //模糊类型
    protected function like($k,$c){
        // 模糊查询 //TODO:目前按全局模糊
        if(!empty($c[$this->key]))
            $this->condition[$k] = [ "like" , "%".$c[$this->key]."%" ];
    }

    //模糊查询  以逗号分割 "abc,abc"
    public function like_continuous($k,$c) {
        if ( !empty($c[$this->key])) {
            if (strpos($c[$this->key], ',')) {
                $array = explode(',', $c[$this->key]);
                $data = [];
                foreach ($array as $v)
                    $data[] = '%' . $v . '%';
                $this->condition[$k] = [
                    'like',
                    $data,
                    'or'
                ];
            } else {
                $this->condition[ $k ] = $c[$this->key];
            }
        }
    }

    // 模糊查询数据表的所有字段 OR
    public function like_all($k,$c) {
        if ( !empty($c[$this->key])) {
            $delete = ['create_time','appointment_time','registration_time','sex'];
            new CrmZbBase();
            $fields_arr = Db::getTableInfo($this->table, 'fields');
            foreach ($fields_arr as $key => $value ) {
                if (in_array($value,$delete))
                    unset($fields_arr[$key]);
            }
            $fields_str = '';
            foreach ($fields_arr as $value)
                $fields_str .= $value.'|';
            $fields_str = trim( $fields_str ,'|' );
            $this->condition[ $fields_str ] = ['like','%'.$c[$k].'%' ];
        }
    }


    /**
     * 模糊查询 如果有中文的话就查询客户的name 否侧就模糊id 和 phone
     * @param $k
     * @param $c
     */
    public function new_like($k,$c) {
        if (!empty($c[$this->key])) {
            if (preg_match_all("/^([\x81-\xfe][\x40-\xfe])+$/",$c[$this->key], $match))
                $field = 'name';
            else
                $field = 'id|phone';
            $this->condition[ $field ] = [ "like" , "%".$c[$this->key]."%" ];
        }
    }


    //恒等类型
    protected function eq($k,$c){
        // 模糊查询 //TODO:目前按全局模糊
        if(!empty($c[$this->key]))
            $this->condition[$k] = $c[$this->key];
    }

    //开始解析
    public function invokeParse($rule,$condition){
        foreach($rule as $key => $value){
            $_rule = $value;
            $this->key = $key;
            if(is_array($value)){
                $key = empty($value['alisa']) ? $key : $value['alisa'];
                $_rule = $value['rule'];
            }
            if(method_exists($this,$_rule))
                $this->$_rule($key,$condition);
        }
        return $this->condition;
    }
}