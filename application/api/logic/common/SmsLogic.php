<?php
/**
 * Created by PhpStorm.
 * User: Paul Walker
 * Date: 2017/3/12
 * Time: 11:56
 */

namespace app\api\logic\common;


use app\api\model\SmsLog;
use app\common\logic\Logic;
use think\Cache;

class SmsLogic extends Logic {

    private $__send_data;   //发送请求的数据
    private $__key = 'SDK-BBX-010-23014';
    private $__pwd = '971403';
    private $__sms_content = [ //发送短信内容
        'login_content' => '您正在登录好样crm系统，验证码为XXXX【好样crm】'
    ];

    /**
     * @var SmsLo
     */
    private $__sms_model;

    function init($val = '')
    {
        $this->__sms_model = new SmsLog();
        // TODO: Implement init() method.
    }


    /**
     * 初始化请求数据
     * @param $phone
     * @param $msg
     */
    private function __initialize($phone, $msg) {
        $send_data = array(
            'sn' => $this->__key,
            'pwd' => strtoupper(md5($this->__key.$this->__pwd)),
            'mobile' => $phone,
            'content' => $msg,
            'ext' => '',
            'stime' => '',
            'msgfmt' => '',
            'rrid' => '',
        );
        $this->__send_data = $send_data;
    }

    /**
     * 构造请求url
     * @return bool
     */
    private function __setUrl() {
        $flag = 0;
        $params = '';
        $argv = $this->__send_data;
        foreach ($argv as $key => $value) {
            if ($flag != 0) {
                $params .= "&";
                $flag = 1;
            }
            $params .= $key . "=";
            $params .= urlencode($value);// urlencode($value);
            $flag = 1;
        }
        $length = strlen($params);
        //创建socket连接
        $fp = fsockopen("sdk.entinfo.cn", 8061, $errno, $errstr, 10) or exit($errstr . "--->" . $errno);
        //构造post请求的头
        $header = "POST /webservice.asmx/mdsmssend HTTP/1.1\r\n";
        $header .= "Host:sdk.entinfo.cn\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Content-Length: " . $length . "\r\n";
        $header .= "Connection: Close\r\n\r\n";
        //添加post的字符串
        $header .= $params . "\r\n";
        return $this->__request($fp, $header);
    }

    /**
     * 发送请求
     * @param $fp
     * @param $header
     * @return array|bool
     */
    private function __request($fp, $header) {
        fputs($fp, $header);
        $inheader = 1;
        $line = "";
        while (!feof($fp)) {
            $line = fgets($fp, 1024); //去除请求包的头只显示页面的返回数据
            if ($inheader && ($line == "\n" || $line == "\r\n")) {
                $inheader = 0;
            }
            if ($inheader == 0) {
                // echo $line;
            }
        }
        $line = str_replace("<string xmlns=\"http://tempuri.org/\">", "", $line);
        $line = str_replace("</string>", "", $line);
        $result = explode("-", $line);
        if (count($result) > 1)
            return $this->returnData('请联系管理员',-6);
        return true;
    }

    /**
     * 推送短信
     * @param $phone
     * @param $action
     * @return array
     */
    public function send($phone,$action,$code = '') {
        if (! preg_match("/^(((13[0-9]{1})|(14[0-9]{1})|(17[0-9]{1})|(15[0-3]{1})|(15[5-9]{1})|(18[0-9]{1}))+\d{8})$/", $phone))
            return ['errCode' => -6,'errMsg' => '手机号码错误'];
        $msg = $this->__sms_content[$action."_content"];
        if(empty($msg))
           return ['errCode' => -6,'errMsg' => '参数错误'];
        $msg = str_replace('XXXX',$code,$msg);
        $msg = htmlspecialchars($msg);
        $this->__initialize($phone, $msg);
        if(($return = $this->__setUrl()) === true){
            $this->__sms_model->save([
                'phone' => $phone,
                'action'=> $action,
                'content'=> $msg
            ]);
        }
        Cache::set('send_sms'.$phone,$code,60);
        return $this->returnData('发送成功');
    }


}