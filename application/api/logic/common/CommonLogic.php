<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 17:05
 */

namespace app\api\logic\common;


use app\api\model\Login;
use app\common\logic\Logic;
use think\Cache;

class CommonLogic extends Logic
{

    function init($val = '')
    {
        // TODO: Implement init() method.
    }

    private function logic( $key ) {
        $obj = [
            'sms' => new SmsLogic()
        ];
        return empty($obj[ $key ]) ? '' : $obj[ $key ];
    }

    private function model( $key ) {
        $model = [
            'login' => new Login()
        ];
        return empty($model[ $key ]) ? '' : $model[ $key ];
    }

    /**
     * 发送短信验证码logic
     * @param $phone
     * @param $action
     * @return array
     */
    public function sendCodeLogic( $phone,$action )
    {
        $login_info = $this->model('login')->where( ['oatuh_id' => $phone,'oatuh_name' => 'account'] )->value('id');
        if ( empty($login_info) )
            return $this->returnData('账号不存在',-6);
        return $this->logic('sms')->send( $phone,$action,rand(1000,9999) );
    }
}