<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/8
 * Time: 10:51
 */

namespace app\api\logic\customer;


use app\api\model\CustomerInfo;
use app\common\logic\Logic;
use traits\think\Instance;

class OperationLogic extends Logic
{
    use Instance;
    /**
     * @var CustomerInfo
     */
    protected $model;

    function init($val = '')
    {
        $this->model = new CustomerInfo();
    }
}