<?php
/**
 * Created by PhpStorm.
 * user: huanghongjiang
 * Date: 2017/6/6
 * Time: 19:19
 */

namespace app\api\logic\customer;

use app\api\logic\common\ParseCondition;
use app\api\logic\user\Admin;
use app\api\model\CustomerInfo;
use app\common\logic\Logic;
use traits\think\Instance;

class ListLogic extends Logic
{

    use Instance;

    //默认筛选规则
    protected $rule = [
        'customer_status' => 'eq',
        'phone' => 'like',
        'id' => 'eq',
        'real_name' => 'like',
        'global' => 'new_like'
    ];

    //列表默认筛选条件
    protected $self_condition = [];

    //默认查的字段
    protected $field = ['id', 'user_id', 'phone', 'sex', 'customer_status', 'province', 'city', 'region', 'address', 'housing_address', 'name'];


    /**
     * @var CustomerInfo
     */
    protected $model;

    function init($val = '')
    {
        $this->model = new CustomerInfo();
        // TODO: Implement init() method.
    }

    /**
     * 清空字段筛选条件并且重新赋值
     * @param array $condition
     */
    protected function emptyRule(array $condition)
    {
        $this->rule = [];
        $this->rule = $condition;
    }

    /**
     * 增加筛选字段
     * @param array $condition
     */
    protected function addRule(array $condition)
    {
        foreach ($condition as $key => $value)
            $this->rule[$key] = $value;
    }

    /**
     * 增加查的字段
     * @param array $field
     */
    protected function addField(array $field)
    {
        $this->field = array_merge($this->field, $field);
    }

    /**
     * 列表入口
     * @param $param
     * @param $action
     * @return array
     */
    public function performAction($param, $action)
    {
        $this->$action();
        return $this->returnData($this->listLogic($param));
    }


//    public function demo() {
    //默认规则
    //$this->self_condition = [ 'customer_status' => 1 ];
    //如果要添加新的筛选字段
    //$this->addRule();
    //如果要重新加新的筛选字段
    //$this->emptyRule();
//    }

    protected function newCustomerLogic()
    {
        $this->self_condition = ['customer_status' => 1];
    }


    protected function effectiveCustomerLogic()
    {
        $this->self_condition = ['customer_status' => 2];
    }


    protected function invalidCustomerLogic()
    {
        $this->self_condition = ['customer_status' => 3];
    }

    protected function todayAccessCustomerLogic()
    {
        $this->self_condition = [
            'back_acces_time_start' => date('Y-m-d', time()),
            'back_acces_time_end' => date('Y-m-d', time() + 86400)
        ];
        $this->addRule(['back_acces_time' => 'time']);
    }

    protected function allCustomerLogic()
    {

    }

    /**
     * 列表查询入口
     * @param $param
     * @param $rule
     * @return \app\common\model\Page|array|string|\think\Model
     */
    protected function listLogic($param)
    {
        if (Admin::role_id() != 1) {
            if (Admin::role_id() == 2) {
                $this->model->changePlusCondition(['store_id' => Admin::store_id()]);
            }
            if (Admin::role_id() == 3) {
                $this->model->changePlusCondition(['user_id' => Admin::id()]);
            }
        }
        if ($param['order']) {

        }
        $where = empty($this->self_condition) ? $param['condition'] : array_merge($this->self_condition, $param['condition']);
        $where = ParseCondition::instance()->invokeParse($this->rule, $where);
        if (!empty($param['content'])) {
            $where[] = function ($q) use ($param) {
                $q->whereOr('phone', 'like', "%" . $param['content'] . "%");
                $q->whereOr('id', 'like', "%" . $param['content'] . "%");
                $q->whereOr('name', 'like', "%" . $param['content'] . "%");
            };
        }
        $where['status'] = 1;
        $data = $this->model->changeFields($this->field)->getList($where, empty($param['page']) ? 1 : $param['page']);
        return $data;
    }

}