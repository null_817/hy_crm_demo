<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 10:00
 */

namespace app\api\logic\customer;


use app\api\logic\user\Admin;
use app\api\model\CustomerInfo;
use app\common\logic\Logic;
use traits\think\Instance;

class DetailsLogic extends Logic
{
    use Instance;

    /**
     * @var CustomerInfo
     */
    protected $model;

    function init($val = '')
    {
        $this->model = model('CustomerInfo');
    }

    /**
     * 客户详情
     * @param $id
     * @return array
     */
    public function getDetailLogic($id)
    {
        $this->model->with(['customer_follow', 'customer_this_status', 'user_name']);
        $data = $this->model->getOne(['id' => $id]);
        return $this->returnData($data);
    }

    /**
     * 软删除客户处理
     * @param $content_id
     * @return array
     */
    public function getDelCustomer($content_id)
    {
        $data    = $this->model->where('id', $content_id)->find();
        $user_id = Admin::id();
        if ($data['user_id'] == $user_id) {
            if ($data['status'] != 0) {
                $data['status'] = 0;
                $data->save();
                return $this->returnData('亲，你已成功抛弃此用户');
            } else {
                return $this->returnData('亲，你早已抛弃了这个用户', -6);
            }
        } else {
            return $this->returnData('亲，这可不是你的客户', -6);
        }

    }
}