<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace app\api\logic;

use Doctrine\Common\Cache\PhpFileCache;
use Thenbsp\Wechat\Wechat\AccessToken;
use think\Cache;
use traits\think\Instance;

class WeiChat
{
    use Instance;

    protected $app_id;

    protected $app_secret;

    public function __construct()
    {
        $this->app_id = config("app_id");
        $this->app_secret = config("app_secret");
    }

    /**
     * 获取缓存驱动
     * @return \Doctrine\Common\Cache\Cache
     */
    public function getCacheDriver(): \Doctrine\Common\Cache\Cache
    {
        return new PhpFileCache(RUNTIME_PATH . "/cache");
    }

    /**
     * 获取令牌
     * @return AccessToken
     */
    public function accessToken(): AccessToken
    {
        $access_token = new AccessToken($this->app_id, $this->app_secret);
        $access_token->setCache($this->getCacheDriver());
        return $access_token;
    }
}