<?php
/**
 * Created by PhpStorm.
 * User: xiaobao
 * Date: 2017/6/7
 * Time: 14:13
 */

namespace app\api\model;

use think\Model;

class CustomerTransfer extends Model
{
    protected $table = 'hy_customer_transfer';
    protected $field = [
        'user_id',
        'current_user_id',
        'transfer_user_id',
        'content',
        'create_time',
        'status'
    ];
}
