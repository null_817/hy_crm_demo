<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/8
 * Time: 11:48
 */

namespace app\api\model;


use app\common\model\Base;

class Store extends Base
{

    protected $append = ['province_name','city_name','region_name'];

    public function getProvinceNameAttr() {
        return get_areas_name( empty($this->{'province'}) ?: $this->{'province'} );
    }

    public function getCityNameAttr() {
        return get_areas_name( empty($this->{'city'}) ?: $this->{'city'} );
    }

    public function getRegionNameAttr() {
        return get_areas_name( empty($this->{'region'}) ?: $this->{'region'} );
    }
}