<?php
/**
 * Created by PhpStorm.
 * User: xiaobao
 * Date: 2017/6/7
 * Time: 14:13
 */

namespace app\api\model;

use think\Model;

class UserModel extends Model
{
    protected $table = 'hy_user';
    protected $field = [
        'role_id',
        'pid',
        'user_name',
        'real_name',
        'email',
        'create_time',
        'update_time',
        'status'
    ];
}
