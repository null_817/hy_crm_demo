<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 10:10
 */

namespace app\api\model;

use app\api\logic\user\Admin;
use app\api\model\User;
use app\common\model\Base;

class CustomerFollow extends Base
{
    protected $append = ['user_name'];

    protected static function init()
    {
        CustomerFollow::event("after_insert", function ($user) {
            $customer = CustomerInfo::where("id", $user->customer_id)->find();
            $customer->update_time = time();
            $customer->back_acces_time = 0;
            $customer->save();
        });
    }

    public function getUserNameAttr()
    {
        if (!empty($this->{'user_id'})) {
            if (Admin::id() == $this->{'user_id'})
                return '我';
            return User::where(['id' => $this->{'user_id'}])->value('real_name');
        }
        return "总部";
    }

    public function getCreateTimeAttr($value)
    {
        return empty($value) ?: date('Y.m.d H:i', $value);
    }

    public function getUpdateTimeAttr($value)
    {
        return empty($value) ?: date('Y.m.d H:i', $value);
    }

}