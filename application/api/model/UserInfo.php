<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/7
 * Time: 15:38
 */

namespace app\api\model;


use app\common\model\Base;

class UserInfo extends Base
{
    protected $table = 'hy_user';

    protected $with = ['role_name'];

    public function roleName()
    {
        return $this->hasOne('role', 'id', 'role_id')->where('status', 1)->field('name,id');
    }

    public function store()
    {
        return $this->hasOne('store', 'id', 'store_id')->where('status', 1)->field(['id', 'province', 'city', 'region', 'address']);
    }

    public function getTypeNameAttr()
    {
        $data = [1 => '正常分配', 2 => '审核中', 3 => '不分配', 4 => '不分配'];
        return empty($data[$this['type']])
            ? '' : $data[$this['type']];
    }
}