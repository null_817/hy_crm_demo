<?php
/**
 * Created by PhpStorm.
 * User: huanghongjiang
 * Date: 2017/6/6
 * Time: 19:32
 */

namespace app\api\model;


use app\common\model\Base;

class CustomerInfo extends Base
{
    protected $autoWriteTimestamp = true;

    protected $field = [
        "id",
        'user_id',
        'customer_status',
        'form',
        'name',
        'sex',
        'form_status',
        'img_pingzheng',
        'phone',
        'phone_note',
        'age',
        'wechat',
        'province',
        'city',
        'region',
        'address',
        'housing_address',
        'last_time',
        'back_acces_time',
        'create_time',
        'update_time',
        'huihua_id',
        'status'
    ];

    protected $append = [
        'province_name', 'city_name', 'region_name'
    ];

    //跟进记录
    public function customerFollow()
    {
        return $this->hasMany('CustomerFollow', 'customer_id', 'id')->where('status', 1);
    }

    //当前状态
    public function customerThisStatus()
    {
        return $this->belongsToMany('CurrentStatus', 'current_customer', 'current_status_id', 'customer_id');
    }

    public function userName()
    {
        return $this->hasOne('User', 'id', 'user_id')->where('status', 1)->field('real_name,id');
    }

    public function getCustomerStatusNameAttr()
    {
        $data = [1 => '新增客户', 2 => '有效客户', 3 => '无效客户'];
        return empty($data[$this->{'customer_status'}]) ? '' : $data[$this->{'customer_status'}];
    }

    public function getProvinceNameAttr()
    {
        return get_areas_name($this->{'province'});
    }

    public function getCityNameAttr()
    {
        return get_areas_name($this->{'city'});
    }

    public function getRegionNameAttr()
    {
        return get_areas_name($this->{'region'});
    }

    public function getSexNameAttr()
    {
        return get_sex_name($this->{'sex'});
    }

    public function getLastTimeAttr($value)
    {
        return new_date($value);
    }

    public function getBackAccesTimeAttr($value)
    {
        return new_date($value);
    }

}