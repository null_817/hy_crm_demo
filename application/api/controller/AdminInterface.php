<?php
namespace app\api\controller;


use app\api\logic\common\DocParser;
use app\api\logic\user\Admin;
use app\LZCompressor\LZString;
use ErrorException;
use think\Cache;
use think\Config;
use think\Request;

/**
 * AdminInterface
 * Class AdminInterface
 * @package app\api\controller
 * @author  albert
 */
final class AdminInterface
{

    /**
     * This is interface param
     * @var array
     */
    protected $param = [];

    /**
     * api controller
     * controller
     * @var
     */
    protected $controller;

    protected $error;

    /**
     * @var \ReflectionMethod
     */
    protected $reflect;

    protected $debug = true;

    /**
     * controller action
     * method
     * @var Method
     */
    protected $action;

    public function __construct()
    {
        $this->debug = Config::get('app_debug');
    }

    /**
     * 执行接口
     * @return array
     */
    private function invokeMethod()
    {
        if (empty($this->controller))
            return ['errCode' => '-2', 'errMsg' => 'error : interface is not defined', 'data' => []];
        if (!method_exists($this->controller, $this->action))
            return ['errCode' => '-3', 'errMsg' => 'error : action undefined (method_exists)', 'data' => []];

        //建立反射对象
        $reflect = $this->reflect = new \ReflectionMethod($this->controller, $this->action);
        //api 接口必需参数检测
        if ($reflect->getNumberOfParameters() > 0) {
            $arguments = $reflect->getParameters();
            $arguments = $arguments[0]->getDefaultValue();
            //print_r($this->param);
            foreach ($arguments as $key => $val) {
                if (!array_key_exists($val, $this->param))
                    return ['errCode' => '-7', 'errMsg' => '$' . $val . ' is not defined'];
            }
        }
        try {
            $action = $this->action;
            $response = $this->controller->$action($this->param);
        } catch (ErrorException $e) {
            $response = ['errCode' => '-3', 'errMsg' => 'error : action  undefined'];
            if (true === $this->debug) $response['data'] = $e->getMessage() . $e->getFile() . $e->getLine();
        }
        return $response;
    }

    /**
     * @interface_name 测试API
     * @description 这个接口是测试
     * @validate name require
     */
    public function debug()
    {
        dump(rand(1000, 9999));
    }


    /**
     * 后端统一入口
     * @param  array $debug_param
     * @return \think\response\Jsonp
     */
    public function rootInterface($debug_param = [])
    {
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Methods:POST,GET,OPTIONS,DELETE,PUT");

        $param = Request::instance()->param("params");
        $interface = Request::instance()->param("interface");
        if ($interface != "PublicAction/importOppeinData") {
            $param = base64_decode(LZString::decompressFromBase64(base64_decode($param)));
        }
        if (!empty($debug_param)) {
            $param = $debug_param['params'];
            $interface = $debug_param['interface'];
        }
        $param = json_decode($param, true);
        $interface = explode('/', $interface);
        if (empty($interface[0] || empty($interface[1])))
            return jsonp(['errCode' => -7, 'errMsg' => 'interface error', 'data' => '']);

        //此处定义常量 注意之后代码与其强耦合
        define('INTERFACE_NAME', $interface[0]);
        define('ACTION_NAME', $interface[1]);
        if ($this->beginAction($param) === false) {
            $response = $this->error;
        } else {
            $controller = controller($interface[0]);
            $this->controller = $controller;
            $this->action = $interface[1];
            $this->setParam($param);
            try {
                $response = $this->invokeMethod();
            } catch (ErrorException $e) {
                $response = ['errCode' => -6, 'errMsg' => 'debug is false not show detail,please contact admin open debug module'];
                if ($this->debug) $response['errMsg'] = $e->getMessage() . $e->getFile() . $e->getLine();
            }
        }
        $this->afterAction($response);
        return jsonp($response);
    }

    /**
     * this is a debug action
     * @return \think\response\Jsonp
     */
    public function debugInterface()
    {
//        $interface = "common/getabc";
        $interface = "User/shiftCustomer";
//        $interface = "Customer/newCustomerList";
//        $interface = "Common/login";

        $param = [
            'name' => 'demo',
            'user_name' => 'admin',
            'phone' => 15626104771,
            'account' => 15626104771,
            'password' => 15626104771,
            'action' => 'login',
            'id' => 3,
            'content_id' => 1,
            'store_id' => 1,
            'user_id' => 10,
            'content' => '懒癌症末期',
            '_token_' => "49925953ffb5da9e02ab16e0b401641da0bf3e9c",
            //            'name' => '郑',
            //            'id' => 9,
            //            'stores_number' => 'AH103',
            //            'id' => 'LS101',
            //            'beginman' => '经手人',
            //            'emddt' => '2017-03-21 16:57:51',
            //            'follow' => '客户无',
            //            'condition' => [],
            //            'username' => 13632605016,
            //            'password' => 'albert',
            'condition' => [
            ]
        ];


        $param = [
            'interface' => $interface,
            'params' => json_encode($param)
        ];
        $response = $this->rootInterface($param);
        return $response;
    }

    public function __get($name)
    {
        // TODO: Implement __get() method.
        if (!empty($this->{$name}))
            return $name;
    }

    //前置操作
    public function beginAction($param)
    {
        $interface = INTERFACE_NAME;
        $action = ACTION_NAME;

        $param['_dd_user_id_'] = '062529691640342468';

        if ($param['_dd_user_id_']) {
            define('DD_USER_ID', $param['_dd_user_id_']);
        }
        $not_validate_arr = Config::get('not_validate_arr');
        if (!array_key_exists('id', $param) || !array_key_exists('_toke_', $param)) {
            if (in_array($interface, $not_validate_arr))
                return true;
        }
        $admin_info = $this->validation($param);
        if ($admin_info === false) {
            return $admin_info;
        }
        return true;
        if ($admin_info['user_name'] === 'admin' || in_array($interface, $not_validate_arr))
            return true;
        if (!Admin::validationNode($interface, $action))
            return $this->setError(['errCode' => -8, 'errMsg' => '权限不足', 'data' => '']);
        return true;
    }

    //后置操作
    public function afterAction($response)
    {
        //记录管理员日志
        $not_arr = ['getAdminMenu', 'getAdminInfo'];
        if (!in_array(ACTION_NAME, $not_arr) && !empty($admin_info)) {
            $log_data = [
                'create_time' => THINK_START_TIME,
                'interface' => INTERFACE_NAME,
                'action' => ACTION_NAME,
                //如果admin::id空 证明这次请求的id有问题,this->param['id']记录下来，否侧取正常的admin:id
                'user_id' => empty(Admin::id()) ? (empty($this->param['id']) ? 0 : $this->param['id']) : Admin::id(),
                'code' => empty($response['errCode']) ? '' : $response['errCode'],
                'msg' => empty($response['errMsg']) ? '' : $response['errMsg'],
                'ip' => $_SERVER['REMOTE_ADDR'],
                'describe' => $this->getActionDesigner(),
                'params' => json_encode($this->param)
            ];
            db('user_log')->insert($log_data);
            //记录管理员操作日志
        }
    }


    public function validation($param)
    {
        if (empty($param['id']) || empty($param['_token_']))
            return $this->setError(['errCode' => -6, 'errMsg' => '参数错误', 'data' => '']);
        $admin_info = Cache::get('user_info' . $param['id']);
        if (empty($admin_info))
            return $this->setError(['errCode' => -4, 'errMsg' => '请先登录', 'data' => '']);
        if ($param['_token_'] !== $admin_info['_token_'])
            return $this->setError(['errCode' => -4, 'errMsg' => '身份失效,请重新登录', 'data' => '']);
        Admin::setUserInfo($admin_info);
        return $admin_info;
    }

    public function setError($error)
    {
        $this->error = $error;
        return false;
    }

    public function getActionDesigner()
    {
        $designer = ' method not ';
        if (!empty($this->reflect)) {
            $new_designer = Cache::get('designer' . INTERFACE_NAME . ACTION_NAME);
            if (empty($new_designer)) {
                $parser = new DocParser();
                $doc = $this->reflect->getDocComment();
                $new_designer = $parser->parse($doc);
                $new_designer = empty($new_designer['description']) ? ' ' : $new_designer['description'];
                Cache::set('designer' . INTERFACE_NAME . ACTION_NAME, $new_designer, 86400);
            }
            $designer = $new_designer;
        }
        return $designer;
    }

    public function setParam($param)
    {
        if (!empty($param))
            $this->param = $param;
    }
}