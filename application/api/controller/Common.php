<?php
namespace app\api\controller;


use app\api\logic\common\CommonLogic;
use app\api\logic\user\LoginLogic;
use app\api\model\CustomerInfo;
use app\api\logic\user\Admin;
use EasyDingTalk\Application;
use Thenbsp\Wechat\Wechat\Jsapi;

class Common
{
    /**
     * @var CommonLogic
     */
    public $logic;

    public function __construct()
    {
        $this->logic = new CommonLogic();
    }

    /**
     * 账号密码登陆
     * @param array $param
     * @return array
     */
    public function login($param = ['account', 'password'])
    {
        return LoginLogic::instance()->userLogic($param['account'], $param['password']);
    }

    public function bigdata($params = [])
    {
        //统计时间
        //如果没传默认当月
        $start_time = $params['start_time'] ?? date('Y-m-01', strtotime(date("Y-m-d")));
        $end_time = $params['end_time'] ?? date('Y-m-d', strtotime("$start_time +1 month -1 day"));

        $model = new CustomerInfo();
        if (Admin::role_id() == 2) {
            $model->where(['store_id' => Admin::store_id()]);
        }
        if (Admin::role_id() == 3) {
            $model->where(['user_id' => Admin::id()]);
        }
        $UnassignedModel = new CustomerInfo();
        $UnassignedModel->where(['store_id' => Admin::store_id()]);
        $to_do = [
            'noVisitToday' => $model->where(["back_acces_time" => [['>=', time()], ['<=', time() + 86400]]])->count('*'),
            'Unassigned' => $UnassignedModel->where('user_id', 0)->count('*')
        ];
        //今日回访客户数

        $newCountModel = new CustomerInfo();
        if (Admin::role_id() == 2) {
            $newCountModel->where(['store_id' => Admin::store_id()]);
        }
        if (Admin::role_id() == 3) {
            $newCountModel->where(['user_id' => Admin::id()]);
        }
        $newCountModel->where([
            "create_time" => [
                ['>=', strtotime($start_time)],
                ['<=', strtotime($end_time)]
            ]
        ]);

        $dayAvgCountModel = clone $newCountModel;
        $validityModel = clone $newCountModel;
        $notValidityModel = clone $newCountModel;
        $yiliangchiModel = clone $newCountModel;
        $yijindian = clone $newCountModel;
        //销售简报
        $sales = [
            'newCount' => $newCountModel->where("customer_status", 1)->count("*"),
            'dayAvgCount' => floor($dayAvgCountModel->where("last_time", 'neq', 0)->count("*") / floor((strtotime($end_time) - strtotime($start_time)) / 86400)),
            'validity' => $validityModel->where("customer_status", 2)->count("*"),
            'notValidity' => $notValidityModel->where("customer_status", 3)->count("*"),
            'yijindian' => $yijindian->where("id",
                'in', function ($q) {
                    $q->name('current_customer')
                        ->where('current_status_id', 6)
                        ->field('customer_id');
                }
            )->count("*"),
            'yiliangchi' => $yiliangchiModel->where("id",
                'in', function ($q) {
                    $q->name('current_customer')
                        ->where('current_status_id', 4)
                        ->field('customer_id');
                }
            )->count("*")
        ];


        $tixing = new CustomerInfo();
        if (Admin::role_id() == 2) {
            $tixing->where(['store_id' => Admin::store_id()]);
        }
        if (Admin::role_id() == 3) {
            $tixing->where(['user_id' => Admin::id()]);
        }

        //36288000
        //
        $twoMonthModel = clone $tixing;
        $oneTwoM = clone $tixing;
        $twoMoreMonth = clone $tixing;

        ////遗忘客户提醒
        $tixing = [
            'oneTwo' => $tixing->where([
                "last_time" => [
                    [">=", time() + 36288000 * 2],
                    ["<=", time() + 36288000]
                ]
            ])->count("*"),
            'twoMonth' => $twoMonthModel->where([
                "last_time" => [
                    [">=", time() + 36288000 * 4],
                    ["<=", time() + 36288000 * 2]
                ]
            ])->count("*"),
            'oneTwoM' => $oneTwoM->where([
                "last_time" => [
                    [">=", time() + 36288000 * 8],
                    ["<=", time() + 36288000 * 4]
                ]
            ])->count("*"),
            'twoMoreMonth' => $twoMoreMonth->where("last_time", ">=", time() + 36288000 * 8
            )->count("*")
        ];


        return [
            'errCode' => 0,
            'errMsg' => "ok",
            'data' => [
                'pk_Array' => [],
                'start_time' => $start_time,
                'end_time' => $end_time,
                'toDo' => $to_do,
                'sales' => $sales,
                'tixing' => $tixing
            ]
        ];
    }

    public function area($param = [])
    {
        $parend_id = $param['p_id'];
    }

    /**
     * 短信验证码登陆
     * @param array $param
     * @return array
     */
    public function verificationCodeLogin($param = ['phone', 'code'])
    {
        return LoginLogic::instance()->verificationCodeLogin($param['phone'], $param['code']);
    }

    /**
     * 发送短信验证码
     * @param array $param
     * @return array
     */
    public function sendCode($param = ['phone', 'action'])
    {
        return $this->logic->sendCodeLogic($param['phone'], $param['action']);
    }

    public function test($params = ['user_id'])
    {
        $user_list = [
            '5267166637856552',
            $params['user_id'],
            '101221626321049791',
            '04483751421181681'
        ];
        $app = new Application([
            "corp_id" => config("corpid"),
            "corp_secret" => config("corpsecret"),
        ]);

        $huihua = $app->chat->create([
            'name' => '测试跟进反馈会话（系统自动新建）',
            'owner' => $params['user_id'],
            'useridlist' => $user_list
        ]);

        return [
            "errCode" => 0,
            "errMsg" => 'ok',
            "data" => $huihua
        ];
    }

    /**
     * 获取微信jsapi config
     * @param array $params
     * @return array
     */
    public function wxJsApi($params = ["jsApiList"])
    {
        $js_api = new Jsapi(\app\api\logic\WeiChat::instance()->accessToken());
        $js_api->setCache(\app\api\logic\WeiChat::instance()->getCacheDriver());
        array_walk($params['jsApiList'], function ($api) use ($js_api) {
            $js_api->addApi($api);
        });
        return [
            "errCode" => 0,
            "errMsg" => "ok",
            "data" => $js_api->getConfig()
        ];
    }

    /**
     * 钉钉jsapi
     * @param array $params
     * @return array
     */
    public function dDJsApi($params = ["jsApiList", "agentId", "url"])
    {
        $app = new Application([
            "corp_id" => config("corpid"),
            "corp_secret" => config("corpsecret"),
        ]);

        return [
            "errCode" => 0,
            "errMsg" => 'ok',
            "data" => $app->jssdk->configBuilder()
                ->ofAgent($params['agentId'])
                ->setUrl(urldecode($params['url']))
                ->useApi($params['jsApiList'])->toArray()
        ];
    }

    /**
     * 钉钉自动登录
     * @param array $params
     * @return array|false|\PDOStatement|string|\think\Model
     */
    public function dDLogin($params = ['user_id'])
    {
        $app = new Application([
            "corp_id" => config("corpid"),
            "corp_secret" => config("corpsecret"),
        ]);

        try {
            $user_detail = $app->user->get($params['user_id']);
        } catch (\Exception $exception) {
            return [
                "errCode" => -6,
                "errMsg" => "您并非本企业人员,请加入本企业再使用该系统.{$exception->getMessage()}"
            ];
        }

        return LoginLogic::instance()->phoneLogin($user_detail['mobile']);
    }
}