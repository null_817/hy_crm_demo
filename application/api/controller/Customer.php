<?php
/**
 * Created by PhpStorm.
 * user: huanghongjiang
 * Date: 2017/6/6
 * Time: 19:13
 */

namespace app\api\controller;

use app\api\logic\customer\DetailsLogic;
use app\api\logic\customer\ListLogic;
use app\api\logic\DingTalk;
use app\api\logic\user\Admin;
use app\api\model\CurrentCustomer;
use app\api\model\CustomerFollow;
use app\api\model\CustomerInfo;
use think\Db;


/**
 * 客户模块
 * @package app\api\controller
 */
class Customer
{

    /**
     * 所有客户列表
     * @param array $param
     * @return array
     */
    public function allCustomerList($param = ['condition'])
    {
        return ListLogic::instance()->performAction($param, 'allCustomerLogic');
    }

    /**
     * 新增客户列表
     * @param array $param
     * @return mixed
     */
    public function newCustomerList($param = ['condition'])
    {
        return ListLogic::instance()->performAction($param, 'newCustomerLogic');
    }

    /**
     * 有效客户表列
     * @param array $param
     * @return mixed
     */
    public function effectiveCustomerList($param = ['condition'])
    {
        return ListLogic::instance()->performAction($param, 'effectiveCustomerLogic');
    }

    /**
     * 无效客户列表
     * @param array $param
     * @return mixed
     */
    public function invalidCustomerList($param = ['condition'])
    {
        return ListLogic::instance()->performAction($param, 'invalidCustomerLogic');
    }


    /**
     * 今日回访客户
     * @param array $param
     * @return array
     */
    public function todayAccessCustomerList($param = [])
    {
        return ListLogic::instance()->performAction($param, 'todayAccessCustomerLogic');
    }

    /**
     * 客户详情
     * @param array $param
     * @return array
     */
    public function getCustomerDetail($param = ['customer_id'])
    {
        return DetailsLogic::instance()->getDetailLogic($param['customer_id']);
    }

    /**
     * 软删除客户
     * @param array $param
     * @return array
     */
    public function delCustomer($param = ['customer_id'])
    {
        return DetailsLogic::instance()->getDelCustomer($param['content_id']);
    }

    /**
     * @param array $param
     *                  => status
     *                  1.新增客户
     *                  2.有效客户
     *                  3.无效客户
     * @return array
     */
    public function status($param = ['customer_id', 'status'])
    {
        $s = [
            1 => '设置客户状态为新增客户',
            2 => '设置客户状态为有效客户',
            3 => '设置客户状态为无效客户'
        ];
        $data = CustomerInfo::where(["id" => $param['customer_id']])->find();
        if (!empty($data)) {
            $data->customer_status = $param['status'];
            $data->save();
            CustomerFollow::create(
                [
                    "user_id" => Admin::id(),
                    "customer_id" => $param['customer_id'],
                    "type" => 3,
                    "content" => $s[$param['status']] ?? "",
                    'create_time' => time()
                ]
            );
            return [
                'errCode' => 0,
                'errMsg' => ''
            ];
        }
    }

    /**
     * @param array $param
     * @return array
     */
    public function money($param = ['customer_id', 'money'])
    {
        CurrentCustomer::where([
            'customer_id' => $param['customer_id'],
            'current_status_id' => [['=', 1], ['=', 2], 'or']
        ])->delete();
        CurrentCustomer::create([
            'customer_id' => $param['customer_id'],
            'current_status_id' => 1
        ]);
        CustomerFollow::create([
            "user_id" => Admin::id(),
            "customer_id" => $param['customer_id'],
            "type" => 2,
            "content" => $param['money'],
            'create_time' => time()
        ]);

        DingTalk::instance()->send($param['customer_id'], "客户已成交,成交金额:" . $param['money']);
        return [
            'errCode' => 0,
            'errMsg' => "ok"
        ];
    }

    public function liangchi($param = ['customer_id'])
    {
        $data = [
            "user_id" => Admin::id(),
            "customer_id" => $param['customer_id'],
            "type" => 1,
            "content" => "登记量尺",
            'create_time' => time()
        ];
        CustomerFollow::create($data);
        CurrentCustomer::where([
            'customer_id' => $param['customer_id'],
            'current_status_id' => [['=', 3], ['=', 4], 'or']
        ])->delete();
        CurrentCustomer::create([
            'customer_id' => $param['customer_id'],
            'current_status_id' => 4
        ]);

        DingTalk::instance()->send($param['customer_id'], "客户已量尺,请悉知");
        return [
            'errCode' => 0,
            'errMsg' => "ok"
        ];
    }

    public function huifang($param = ['customer_id', 'time'])
    {
        $data = CustomerInfo::where(["id" => $param['customer_id']])->find();
        if (!empty($data)) {
            $data->back_acces_time = strtotime($param['time']);
            $data->save();
            CustomerFollow::create(
                [
                    "user_id" => Admin::id(),
                    "customer_id" => $param['customer_id'],
                    "type" => 3,
                    "content" => "设置回访时间" . $param['time'],
                    'create_time' => time()
                ]
            );
            return [
                'errCode' => 0,
                'errMsg' => ''
            ];
        }
    }

    public function jindian($param = ['customer_id'])
    {
        $data = [
            "user_id" => Admin::id(),
            "customer_id" => $param['customer_id'],
            "type" => 1,
            "content" => "进店登记",
            'create_time' => time()
        ];
        CustomerFollow::create($data);
        CurrentCustomer::where([
            'customer_id' => $param['customer_id'],
            'current_status_id' => [['=', 5], ['=', 6], 'or']
        ])->delete();
        CurrentCustomer::create([
            'customer_id' => $param['customer_id'],
            'current_status_id' => 6
        ]);
        DingTalk::instance()->send($param['customer_id'], "客户已进店,请悉知");
        return [
            'errCode' => 0,
            'errMsg' => "ok"
        ];
    }

    public function suijixiafa()
    {
        if (Admin::role_id() != 2) {
            return [
                'errCode' => -6,
                'errMsg' => "只有负责人可以做这个操作",
                'data' => ''
            ];
        }
        $user = Db::table('hy_user')->where('store_id', Admin::store_id())->where('status', 1)->where('type', 1)->column('id,real_name,type', 'id');
        Db::table('hy_customer_info')->where('store_id', Admin::store_id())->where("user_id", 0)->limit(200)->chunk(100, function ($customers) use ($user) {
            foreach ($customers as $customer) {
                $u = $user[array_rand($user)];
                Db::table('hy_customer_info')->where('id', $customer['id'])->update(['user_id' => $u['id']]);
                CustomerFollow::create([
                    "user_id" => Admin::id(),
                    "customer_id" => $customer['id'],
                    "type" => 1,
                    "content" => "随机分配给" . $u['real_name'] . "操作人:" . Admin::real_name(),
                    'create_time' => time()
                ]);
            }
        });
        return ['errCode' => 0, 'errMsg' => 'ok', 'data' => ''];
    }

    public function address($param = ['address', 'customer_id'])
    {
        $user = CustomerInfo::where(['id' => $param['customer_id']])->find();
        if (!empty($user)) {
            CustomerFollow::create([
                "user_id" => Admin::id(),
                "customer_id" => $param['customer_id'],
                "type" => 3,
                "content" => "修改客户地址,修改前" . $user->address . "修改后:" . $param['address'],
                'create_time' => time()
            ]);
            $user->address = $param['address'];
            $user->save();
        }
        return [
            'errCode' => 0,
            'errMsg' => '-6',
            'data' => ''
        ];
    }

    /**
     * 新增客户
     * @param array $param
     * @return array
     */
    public function createCustomer($param = ['phone', 'address', 'name'])
    {
        $data = CustomerInfo::where("phone", $param['phone'])->find();
        if (!empty($data)) {
            return [
                "errCode" => -6,
                "errMsg" => "客户已存在",
                "data" => ""
            ];
        }
        $data = [
            "phone" => $param['phone'],
            "address" => $param['address'],
            "name" => $param['name'],
            "form" => "门店",
            "form_status" => 1,
            "user_id" => Admin::id(),
            "store_id" => Admin::store_id()
        ];
        if (!empty($param['img_pingzheng'])) {
            $data['img_pingzheng'] = $param['img_pingzheng'];
        }
        $customer = CustomerInfo::create($data);
        $model = new CurrentCustomer();
        $model->insertAll([
            [
                'customer_id' => $customer->id,
                'current_status_id' => 2
            ],
            [
                'customer_id' => $customer->id,
                'current_status_id' => 3
            ],
            [
                'customer_id' => $customer->id,
                'current_status_id' => 5
            ]
        ]);
        return [
            "errCode" => 0,
            "errMsg" => "ok",
            "data" => $customer->id
        ];
    }

    /**
     * 添加跟进记录
     * @param array $param
     * @return array
     */
    public function addFollow($param = ['customer_id', 'type', 'content'])
    {
        $data = [
            "user_id" => Admin::id(),
            "customer_id" => $param['customer_id'],
            "type" => $param['type'],
            "content" => $param['content'],
            'create_time' => time()
        ];
        if (!empty($param['zhengyi'])) {
            $data = CustomerInfo::where("id", $param['customer_id'])->find();
            $data['form_status'] = 2;
            $data->save();
//            DingTalk::instance()->send($param['customer_id'], "客户存在争议");
        }
        if ($param['type'] == 4) {
            DingTalk::instance()->send($param['customer_id'], $param['content'], 'img');
        }
        CustomerFollow::create($data);
        if ($param['type'] != 4) {
            DingTalk::instance()->send($param['customer_id'], $param['content']);
        }
        return [
            'errCode' => 0,
            'errMsg' => "ok"
        ];
    }
}