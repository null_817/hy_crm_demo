<?php
/**
 * Created by PhpStorm.
 * user: huanghongjiang
 * Date: 2017/6/6
 * Time: 19:16
 */

namespace app\api\controller;

use app\api\logic\user\Admin;
use app\api\logic\user\UserLogic;
use app\api\model\UserInfo;
use app\common\logic\Logic;
use think\Db;

/**
 * 用户模块
 * @package app\api\controller
 */
class User
{
    /**
     * 获取用户信息
     * @return array
     */
    public function getUserInfo($param = [])
    {
        return UserLogic::instance()->getUserInfoLogic();
    }

    /**
     * 我的团队列表
     * @return array
     */
    public function getTeamList($param = [])
    {
        return UserLogic::instance()->getTeamListLogic();
    }

    /**
     * 添加成员
     * @param array $param
     * @return array
     */
    public function addMember($param = ['name', 'phone'])
    {
        return UserLogic::instance()->getAddMember($param);
    }

    /**
     * 删除成员
     * @param array $param
     * @return mixed
     */
    public function delMember($param = ['id'])
    {
        return UserLogic::instance()->getDelMember($param);
    }

    /**
     * 不分配客户||分配客户
     * @param array $param
     * @return array
     */
    public function allot($param = ['id'])
    {
        if (Admin::role_id() != 1 && Admin::role_id() != 2) {
            return ['errCode' => -6, 'errMsg' => '无权限调整', 'data' => ''];
        }
        $user = UserInfo::where(['id' => $param['id'], 'store_id' => Admin::store_id()])->find();
        if (empty($user)) {
            return ['errCode' => -6, 'errMsg' => '无权限调整', 'data' => ''];
        }
        $user['type'] = $user['type'] == 1 ? 3 : 1;
        $user->save();
        return ['errCode' => 0, 'errMsg' => 'ok', 'data' => $user['type']];
    }

    /**
     * 查询可转移的用户
     * @param array $param
     * @return mixed
     */
    public function mayShiftCustomer($param = [])
    {
        return UserLogic::instance()->getMayShiftCustomer($param);
    }

    /**
     * 转移客户
     * @param array $param
     *              转移 shift_id
     *              接收 user_id
     *              原因 content
     * @return mixed
     */
    public function shiftCustomer($param = ['shift_id', 'user_id', 'content'])
    {
        if (Admin::role_id() != 1 || Admin::role_id() != 2) {
            return ['errCode' => -6, 'errMsg' => '无权限转移'];
        }
        $user = UserInfo::where("id",$param['user_id'])->find();
        $shift = UserInfo::where("id",$param['shift_id'])->find();
        Db::transaction(function () use ($param,$shift,$user) {
            $shift_id = $param['shift_id'];
            $user_id  = $param['user_id'];
            $content  = $param['content'];
            $content  = "原跟进人:".$shift['real_name'] . "转移给:".$user['real_name'] . "备注:".$content;
            $time     = time();
            Db::name('customer_info')->where('user_id', $param['shift_id'])->field("$shift_id,id,$content,1,$time")->selectInsert('user_id,customer_id,content,type,create_time', 'customer_follow');
            Db::name('customer_info')->where('user_id', $param['shift_id'])->update(['user_id' => $param['user_id']]);
        });
        return ['errCode' => 0 , 'errMsg' => 'ok'];
    }
}