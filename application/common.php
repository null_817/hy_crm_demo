<?php
// 命名规则
// get_time
// get_store_name
// get_city_name
// 应用公共文件

/**
 * 在找出来的数据中,任意一个字段的值，在整一列中没有的话,把这个列删除掉
 * @param $data
 * @return mixed
 */
function returnData( $data,$table = '' ) {
    $fields_arr = \think\Db::getTableInfo($table, 'fields');
    foreach ( $fields_arr as $value ) {
        $new_data = array_column($data->list,$value,$value);
        if ( count($new_data) == 1 && (reset($new_data) == '' || reset($new_data) == '0' ) ) {
            foreach ( $data->list as $k => $v ) {
                foreach ( $v as $field => $field_value ) {
                    unset($data->list[ $k ][ $value ]);
                }
            }
        }
    }
    return $data;
}

function get_sex_name( $value ) {
    if ( $value == '1' )
        return '男';
    else
        return '女';
}

function getTime(){
    return $_SERVER['REQUEST_TIME'];
}

function new_date($val , $date_format = 'Y-m-d G:i:s'){
    return empty($val)?'':date($date_format, $val);
}

function is_empty( $value ) {
    return empty( $value ) ? '' : $value;
}

function get_areas_name($areas_id){
    if(empty($areas_id))
        return "";
    $name = \think\Cache::get("areas_".$areas_id);
    if(empty($name)){
        $name = \think\Db::table('dj_system_areas')->where(['id' => $areas_id])->value("area_name");
        if(!empty($name))
            \think\Cache::set("areas_".$areas_id,$name);
    }
    return $name;
}

function get_areas_id( $city_name ) {
    if ( empty( $city_name) )
        return "";
    $name = \think\Cache::get("areas_".$city_name);
    if ( empty( $name) ) {
        $name = \think\Db::table('dj_system_areas')->where(['area_name' => $city_name])->value("id");
        if(!empty($name)) {
            \think\Cache::set( "areas_" . $city_name, $name );
            return \think\Cache::get("areas_".$city_name);
        }
    }
    return $name;
}

//get store_name
function getStoreName($store_id){
    $name = \think\Cache::get('store_key'.$store_id);
    if(empty($name)){
        $name = \think\Db::table('dj_store')->where(['id' => $store_id])->value('name');
        \think\Cache::set('store_key'.$store_id,$name);
    }
    return $name;
}
//get_city_name
function getCityName($city_id) {
    $message_obj = new \app\message\logic\Message();
    return $message_obj ->getCityName($city_id);
}

function getLoginUserData(){
    if(!(defined('_USER_TOKEN_') || defined('_USER_TIME_')  || defined('_USER_ID_')))
        return false;
    $token      = md5(_USER_ID_._USER_TOKEN_._USER_TIME_);
    $user_info  = \think\Cache::get($token);
    if(empty($user_info))
        return false;
    return $user_info;
}

function recursion($list,$pid = 0) {
    $arr = [];
    foreach ($list as $key => $value) {
        if ($value['pid'] == $pid) {
            $value['child'] = recursion($list,$value['id']);
            $arr[] = $value;
        }
    }
    return $arr;
}

/**
 * 组合文章导航标题
 * @param $list
 * @param int $pid
 * @return array
 */
function getHouseholdRecursive( $list, $pid = 0) {
    $arr = [];
    foreach ($list as $key => $value) {
        if ($value['parent_id'] == $pid) {
            $value['child'] = getHouseholdRecursive($list,$value['id']);
            $arr[] = $value;
        }
    }
    return $arr;
}

/**
 * 计算跳转的id
 * @param $fid
 * @param $zid
 * @return string
 */
function setHref( $fid, $zid) {
    $has_arr = ['0','1','2','3','4','5','6','7','8','9'];
    //表示两个id都是个位数
    if (!empty($has_arr[$fid]) && !empty($has_arr[$zid]))
        return '0'.$fid.'0'.$zid;
    //如果父id是个位数，再判断下子id是不是个位数
    else if (!empty($has_arr[$fid])) {
        if (!empty($has_arr[$zid]))
            return '0'.$fid.'0'.$zid;
        return '0'.$fid.$zid;
    }
    //如果子id是个位数，再判断父id是不是个位数
    else if (!empty($has_arr[$zid])) {
        if (!empty($has_arr[$fid]))
            return '0'.$fid.'0'.$zid;
        return $fid.'0'.$zid;
    }
    else {
        return $fid.$zid;
    }
}


/**
 * 加密方法
 * @param string $str
 * @return string
 */
function encrypt($str,$screct_key){
    //AES, 128 模式加密数据 CBC
    $screct_key = base64_decode($screct_key);
    $str = trim($str);
    $str = addPKCS7Padding($str);
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_CBC),1);
    $encrypt_str =  mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $screct_key, $str, MCRYPT_MODE_CBC);
    return base64_encode($encrypt_str);
}

/**
 * 解密方法
 * @param string $str
 * @return string
 */
function decrypt($str,$screct_key){
    //AES, 128 模式加密数据 CBC
    $str = base64_decode($str);
    $screct_key = base64_decode($screct_key);
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128,MCRYPT_MODE_CBC),1);
    $encrypt_str =  mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $screct_key, $str, MCRYPT_MODE_CBC);
    $encrypt_str = trim($encrypt_str);
    $encrypt_str = stripPKSC7Padding($encrypt_str);
    return $encrypt_str;
}

/**
 * 填充算法
 * @param string $source
 * @return string
 */
function addPKCS7Padding($source){
    $source = trim($source);
    $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

    $pad = $block - (strlen($source) % $block);
    if ($pad <= $block) {
        $char = chr($pad);
        $source .= str_repeat($char, $pad);
    }
    return $source;
}

/**
 * 移去填充算法
 * @param string $source
 * @return string
 */
function stripPKSC7Padding($source){
    $source = trim($source);
    $char = substr($source, -1);
    $num = ord($char);
    if($num==62)return $source;
    $source = substr($source,0,-$num);
    return $source;
}