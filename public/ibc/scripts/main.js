/* 
* @Author: anchen
* @Date:   2016-12-12 17:33:15
* @Last Modified by:   anchen
* @Last Modified time: 2016-12-13 22:02:22
*/

$(function() {

	/*
	 * 模仿select下拉列表
	 * modified : 2016.12.17
	 * modified author : zhenjie
	 */
    /*(function() {
        $('.placeBox h2').text( $('.placeBox li').eq(0).text() );

        $('.placeBox li').each(function() {
            $(this).on('click', function() {
                $('.placeBox h2').text( $(this).text() );
            });
        });

        $('.placeBox').on('click', function(ev) {
            $('.placeList').fadeIn(200);
            ev.stopPropagation();
        });

        $('.placeList').on('click', function(ev) {
            $('.placeList').fadeOut(200);
            ev.stopPropagation();
        });

        $(document).on('click', function() {
            $('.placeList').fadeOut(200);
        });
    })();*/

    // 我的兑换券 tab栏切换
    (function() {
        if ( !document.querySelector('#tabs-container1') ) return;
        var tabsSwiper = new Swiper('#tabs-container1',{
            speed:500,
            onSlideChangeStart: function(){
                $(".tabs1 .active").removeClass('active')
                $(".tabs1 a").eq(tabsSwiper.activeIndex).addClass('active')  
            }
        })

        $(".tabs1 a").on('touchstart mousedown',function(e){
            e.preventDefault()
            $(".tabs .active").removeClass('active')
            $(this).addClass('active')
            tabsSwiper.slideTo( $(this).index() )
        })

        $(".tabs1 a").click(function(e){
            e.preventDefault()
        }) 
    })();

    // 我的优惠券 tab栏切换
    (function() {
        if ( !document.querySelector('#tabs-container2') ) return;
        var tabsSwiper = new Swiper('#tabs-container2',{
            speed:500,
            onSlideChangeStart: function(){
                $(".tabs2 .active").removeClass('active')
                $(".tabs2 a").eq(tabsSwiper.activeIndex).addClass('active')  
            }
        })

        $(".tabs2 a").on('touchstart mousedown',function(e){
            e.preventDefault()
            $(".tabs .active").removeClass('active')
            $(this).addClass('active')
            tabsSwiper.slideTo( $(this).index() )
        })

        $(".tabs2 a").click(function(e){
            e.preventDefault()
        }) 
    })();

    // 正在热映，即将上映 tab栏切换
    (function() {
        if ( !document.querySelector('#tabs-container3') ) return;
        var tabsSwiper = new Swiper('#tabs-container3',{
            speed:500,
            onSlideChangeStart: function(){
                $(".tabs3 .active").removeClass('active')
                $(".tabs3 a").eq(tabsSwiper.activeIndex).addClass('active')  
            }
        })

        $(".tabs3 a").on('touchstart mousedown',function(e){
            e.preventDefault()
            $(".tabs3 .active").removeClass('active')
            $(this).addClass('active')
            tabsSwiper.slideTo( $(this).index() )
        })

        $(".tabs3 a").click(function(e){
            e.preventDefault()
        }) 
    })();


    // 电影票订单-订单列表页
    (function() {
        if ( !document.querySelector('#tabs-container4') ) return;
        var tabsSwiper = new Swiper('#tabs-container4',{
            speed:500,
            onSlideChangeStart: function(){
                $(".tabs4 .active").removeClass('active')
                $(".tabs4 a").eq(tabsSwiper.activeIndex).addClass('active')  
            }
        })

        $(".tabs4 a").on('touchstart mousedown',function(e){
            e.preventDefault()
            $(".tabs4 .active").removeClass('active')
            $(this).addClass('active')
            tabsSwiper.slideTo( $(this).index() )
        })

        $(".tabs4 a").click(function(e){
            e.preventDefault()
        }) 
    })();
    
    //banner
    (function() {
        if ( !document.querySelector('#banner') ) return;

        var mySwiper = new Swiper('#banner',{
            autoplay:5000,
            visibilityFullFit : true,
            loop:true,
            pagination : '.pagination',
        });


    })();

    //主页切换
    (function() {

        if ( !document.querySelector('.container') ) return;

        var aDiv = document.querySelectorAll('.container > div');



        var aLi = document.querySelectorAll('footer li');

        for (var i = 0; i < aLi.length; i++) {
            aLi[i].index = i;
        }

        for (var i = 0; i < aLi.length; i++) {
            aLi[i].onclick = function() {
                document.body.scrollTop = 0;
                document.documentElement.scrollTop = 0;
                for (var j = 0; j < aDiv.length; j ++) {
                    aDiv[j].style.display = 'none';
                    aLi[j].className = '';
                }
                aDiv[this.index].style.display = 'block';
                this.className = 'on';
            }
        }


    })();
	 // 文字展开与折叠
    (function() {


        var word = $('.fold').text();

        var foldToggle = function() {

            if ( $('.fold_btn').hasClass('on') ) {
                $('.fold_btn i').text('全部展开');
                if ( ! word.length <= $('.fold').data('word') ) {
                    $('.fold').text( word.slice(0, $('.fold').data('word')) + '......' );
                }
            } else {
                $('.fold_btn i').text('收起');
                $('.fold').text(word);
            }

        };
            
        foldToggle();

        $('.fold_btn').click(function() {
            $(this).toggleClass('on');
            foldToggle();
        });

    })();

    // 剧照展示
    /*(function() {

        var swiper = new Swiper('.swiper-container1', {
            pagination: '.swiper-pagination',
            paginationType: 'fraction'
        });

    })();*/
	 // 剧照切换
    (function() {

        if ( !document.querySelector('.swiper-container3') ) return;

        // 剧照
        var mySwiper = new Swiper('.swiper-container3',{
            paginationClickable: true,
            slidesPerView: 3
        });

        // 剧照弹窗
        var swiper = new Swiper('.swiper-container1',{
            pagination: '.swiper-pagination',
            paginationClickable: true,
            centeredSlides: true,
            slidesPerView: 1,
            watchActiveIndex: true,
            onSlideChangeStart: function(swiper){
                $('.swiper-pagination-current').text(swiper.activeIndex + 1);
            }
        });

        // 设置图片总数
        $('.swiper-pagination-total').text( $('.swiper-container1 .swiper-wrapper').children().length );

        // 点击剧照，切换剧照弹窗的图片
        $(document).on('click', '.swiper-container3 .swiper-wrapper > div', function() {
            $('.swiper-container1 .swiper-pagination').children().eq( $(this).index() ).click();
        });

        // 点击出现
        $(document).on('click', '.swiper-container3 .swiper-slide', function() {
            $('.color_stills').fadeIn();
            $('.color_stills').css('visibility', 'visible');
            
        });

        // 点击消失
        $(document).on('click', '.color_stills .close', function() {
            $('.color_stills').fadeOut();
        });

    })();

    // 影院详情 - 日期切换
    (function(){

        if ( !document.querySelector('.swiper-nav1') ) return;
        
        //Swiper Content
        var contentSwiper = $('.swiper-content1').swiper({
            onSlideChangeStart: function(){
                updateNavPosition()
            }
        })
        //Nav
        var navSwiper = $('.swiper-nav1').swiper({
            visibilityFullFit: true,
            slidesPerView:'auto',
            //Thumbnails Clicks
            onSlideClick: function(){
                contentSwiper.swipeTo( navSwiper.clickedSlideIndex )
            }
        })

        //Update Nav Position
        function updateNavPosition(){
            $('.swiper-nav1 .active-nav').removeClass('active-nav')
            var activeNav = $('.swiper-nav1 .swiper-slide').eq(contentSwiper.activeIndex).addClass('active-nav')
            if (!activeNav.hasClass('swiper-slide-visible')) {
                if (activeNav.index()>navSwiper.activeIndex) {
                    var thumbsPerNav = Math.floor(navSwiper.width/activeNav.width())-1
                    navSwiper.swipeTo(activeNav.index()-thumbsPerNav)
                }
                else {
                    navSwiper.swipeTo(activeNav.index())
                }   
            }
        }
    })();

    // 影院详情 - 电影切换
    (function() {

        if ( !document.querySelector('.swiper-container2') ) return;

        var mySwiper = new Swiper('.swiper-container2',{
            pagination: '.swiper-container2 .pagination',
            paginationClickable: true,
            centeredSlides: true,
            slidesPerView: 5,
            watchActiveIndex: true
        });

        $('.swiper-container2 .swiper-wrapper > div').click(function() {
            $('.swiper-container2 .pagination').children().eq( $(this).index() ).click();
        });
		
		if ($('.swiper-container2 .swiper-wrapper > div').length >= 3) {
			$('.swiper-container2 .swiper-wrapper > div').eq(2).click();
		}
		
    })();


});